\documentclass{sig-alternate}

% \documentclass[letterpaper]{article}
% \usepackage{aaai}
\usepackage[retainorgcmds]{IEEEtrantools}
\usepackage{amssymb, amsmath}
% \usepackage{amsthm}
\usepackage{graphicx}
\usepackage{times}
\usepackage{helvet}
\usepackage{courier}
\usepackage{multirow}
\usepackage{diagbox}

% \usepackage{psfig}
\usepackage{array}
\usepackage{subfigure}
\usepackage{epsfig}
\usepackage{latexsym}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{psfrag}

% \newtheorem{the}{Theorem}
\newtheorem{lem}{Lemma}
\newtheorem{thm}{Theorem}
\usepackage{xcolor}

% added by Chen
\usepackage[%
breaklinks=true,%
% colorlinks=true,%
% urlcolor= black,%
% menucolor=black,%
% citecolor=black,%
% linkcolor=black,%
bookmarks=true,%
hyperfootnotes=true,%
]{hyperref}%
\usepackage{flushend}
% \usepackage{hyperref}
\usepackage{multirow}

% \graphicspath{{regAll/}} 
\graphicspath{{fig/}} 


\def\sharedaffiliation{ \end{tabular} \begin{tabular}{c}}

\begin{document}

% \conferenceinfo{GECCO'12,} {July, 2012, Philadelphia, PA}
% \CopyrightYear{2012}
% \crdata{978-1-60558-131-6/08/07}

\conferenceinfo{GECCO'13,} {July 6--10, 2013, Amsterdam, The Netherlands.}
\CopyrightYear{2013}
\crdata{978-1-4503-1963-8/13/07}
\clubpenalty=10000
\widowpenalty = 10000

\title{An Efficient Computation of Statistical Moments\\ over Hyperplanes
  and its application to MAX-SAT}

\author{ 
  % Paper ID
  \alignauthor Wenxiang Chen\\
  \affaddr{Department of Computer Science}\\
  \affaddr{Colorado State University} \\
  \affaddr{Fort Collins, CO 80523} \\
  \email{chenwx@cs.colostate.edu}
}

\maketitle

\begin{abstract}
  Maximum Satisfiability (MAX-SAT) is a generalization of the widely
  studied Satisfiability (SAT). Since MAX-SAT is NP-hard, incomplete
  solvers such as Stochastic Local Search (SLS) are employed to solve
  MAX-SAT approximately. Structure information of MAX-SAT problem has
  been utilized to improve the performance of SLS. For instance, recent
  study shows using means (first moments) over hyperplanes can be used
  to initialize variable assignment, which leads to better performance
  of SLS. In this paper, we first propose a method for efficiently
  computing any statistical moment over hyperplanes. We then empirically
  demonstrate that second moments over hyperplane can be employed to
  guide search to closer-to-optima regions and also further improve the
  performance of local search.
\end{abstract}

\category{I.2.8}{Artificial Intelligence}{Problem Solving, Control Methods, and Search}
\terms{Theory, Algorithms}
\keywords{Local Search, SAT, MAX-SAT, Hyperplane, Moment}

\section{Introduction}
\label{sec:intr}
Maximum-Satisfiability (MAX-SAT) problem
\cite{Hansen1990Algorithmsmaximumsatisfiability} is the problem of
determining the maximum number of satisfied clauses of a given Boolean
formula. It is one of the optimization extensions of the Boolean
satisfiability problem (SAT)
\cite{Schaefer1978ComplexityofSatisfiability}, which is the problem of
determining if the variables of a given Boolean formula can be assigned
in such a way as to make the formula evaluate to true. MAX-SAT is one of
the most widely studied problems in computer science, and it is
well-known for being first class of problems proved to be NP-Hard
\cite{Garey:1990:CIG:574848}. Any problem that falls into NP-Hard class
can be encoded into MAX-SAT problem, and be solved by a MAX-SAT
solver. It also has a wide variety of real-world applications ranging
from design debugging \cite{Chen2010AutomatedDesignDebugging}, filter
design \cite{pbe2007} to even therapy design \cite{23134738}.
% applications of SAT digital systems \cite{614088}, combinatorics
% \cite{duque2009new} to cryptography
% \cite{Massacci:2000:LCS:594137.594288}.

There are two major search paradigms for solving MAX-SAT:
\textit{systematic search} \cite{Li2007} and \textit{Stochastic Local
  Search} (SLS) \cite{citeulike:815474}. We focus on SLS in the current
paper. SLS typically starts with some initialized candidate solution,
and then iteratively modifies the candidate solution following the
gradient information from the evaluation function. SLS is able to obtain
solutions (which are likely to be suboptimal though) on large instances
(with millions of variables) much faster than complete
solvers. Nevertheless, the major issue faced by SLS is the basins of
attraction that contains local optima \cite{ILSluorenco}, which prevents
SLS from reaching global optima.

In order to alleviate this effect, global information regarding problem
structures has been explored to guide SLS to promising regions of search
space. Instead of pre-running SLS to collect local optima for estimating
problem structure as in paper
\cite{Zhang:2004:CLA:1029276.1029277,Zhang2003BackboneGuidedLocal,qasem2010learning},
Hains \textit{et al.} \cite{hyper2013gecco} compute the mean values over
hyperplanes (also known as hyperplane averages) from Walsh polynomials
without sampling the search space. The mean values are used as scores to
evaluate the potential of candidate hyperplanes so that we can select
top hyperplanes accordingly. The fixed variables in top hyperplanes are
then employed to generate a probability distribution. Finally, the
probability distribution is used to initialize SLS. This process of
constructing the probability distribution by which the variables should
be assigned is called \textit{Hyperplane Voting}.

 % We argue that the mean values (first
% statistical moments). 
Now that mean value is essentially first moment, we conjecture that the
fitness distributions over hyperplanes can be better characterized using
higher order moments. With higher order moments, candidate hyperplanes
containing global optima are more likely to be selected, which in return
results in initialized solutions of higher quality for SLS and may
eventually lead to better performance of SLS.

The contribution of the current paper is two-fold. First, we first
propose an improved algorithm for computing any moments over
hyperplanes.  Assuming $n$ is the number of variables and $r$ is the
order of moment. Our new algorithm runs in $O(n^{(r-1)})$, while the
previous bound is $O(n^{r})$ \cite{heckendorn2002embedded}. The new
bound leads to a constant time (in $n$) computation of first moments
over hyperplanes and linear time computation of second moments over
hyperplanes. Second, we apply the second moments to construct two new
scores in hyperplane voting. We empirically demonstrate that the two new
scores can indeed improve both the evaluations of solutions and reduce
their distances to global optima. However, one should be aware that the
improvement in performance of SLS is at a cost of extra processing time
in hyperplane voting. Subsection~\ref{sse:time} will cover this issue in
detail.

% The organization of the this paper is as follow 

\section{An Improved Computation of Any Moments over Hyperplanes}
\label{sec:imprAlgo}

In this section, we first review several concepts necessary for
understanding our paper, we then introduce the original algorithm for
computing moments over hyperplanes, and our improved algorithm is
discussed lastly.

\subsection{Preliminaries}
\label{sec:prelim}

\subsubsection{Hyperplanes in Search}
\label{sss:hyper}

Let us consider a MAX-SAT instance with $n$ binary variables, $m$
clauses and $k$ as the maximum number of variables per clause.  The
search space is a $n$-dimensional hypercube containing $2^n$
points. Suppose we fix the values of $j$ positions of a variable
assignment, the search space is reduced to a $(n-j)$-dimensional
\textit{hyperplane}, since there are only $(n-j)$ ``free'' positions
left. A wildcard $'*'$ is used to indicate the position can be either
$'0'$ or $'1'$. Two functions defined below are used to uniquely
identify a hyperplane \cite{goldberg1989genetic}.
\begin{equation}
\alpha(h)[i] =
\begin{cases}
0 & \text{if } h[i]=*,\\
1 & \text{if } h[i]=0\text{ or }1.
\end{cases}
\label{equ:alpha}
\end{equation}
\begin{equation}
\beta(h)[i] =
\begin{cases}
0 & \text{if } h[i]=*\text{ or }0,\\
1 & \text{if } h[i]=1.
\end{cases}
\label{equ:beta}
\end{equation}
For a hyperplane $h='*01*'$, $\alpha(h)='0110'$ and $\beta(h)='0010'$.

\subsubsection{Walsh Transform}
\label{sss:walsh}

Walsh analysis is a set of techniques built on the basis of Walsh
transformation \cite{Walsh1923}.  The
Walsh transformation \cite{Walsh1923} (also called Hadamard transform)
is an example of a generalized Fourier Transform \cite{5217220}. It
decomposes any given fitness function $f({x})$ defined over
$\mathcal{B}^n\rightarrow \mathcal{R}$ into a linear combination of
orthonormal bases $\psi_{i}({x})$, where
${i},{x}\in \mathcal{B}^n$, as shown in
Equation~\eqref{eq:walsh}.
\begin{equation}
  \label{eq:walsh}
  f({x}) = \sum_{\forall {i}\in \mathcal{B}^n} w_{i} \psi_{i}({x}) 
\end{equation}
where $w_i\in \mathcal{R}$ is called the \textit{Walsh coefficient} and
$\psi_{i}({x})$ is called the \textit{Walsh function} that
generates a sign as defined in Equation~\eqref{eq:basis}.
\begin{equation}
  \label{eq:basis}
  \psi_{i}({x}) = (-1)^{\mathbf{bc}( {i} \wedge {x})}
\end{equation}
\textbf{bc}({i}) counts the number of 1's in bit string
{i}.  Walsh transformation enables us to compute Walsh
coefficients directly from $f({x})$, as shown in
Equation~\eqref{eq:trans}.
\begin{equation}
  \label{eq:trans}
  w_{i} = \frac{1}{2^n} \sum_{\forall {i}\in \mathcal{B}^n}f({x})\psi_{i}({x})
\end{equation}


The idea of applying Walsh transformation to search dates back to
1980s. Bethke \cite{bethke1981} introduces a direct method to compute
the mean hyperplane fitness value. The direct calculation of the mean
fitness value over hyperplane is significant because genetic algorithms
\cite{citeulike:1281572} compare hyperplanes and allocate more
computational resources to those with higher fitnesses.

\subsubsection{Statistical Moments}
\label{sss:moment}
Moment is a quantitative measure of the shape of a set of points as
defined in equation~\eqref{eq:mom}.
\begin{equation}
  \label{eq:mom}
  \mu_{r}(a) = \sum (x-a)^c P(x),
\end{equation}
where $r$ is the order of moment and $P(x)$ is the probability mass
function for discrete random variable $x$. Let $a=0$, we have
\textit{ram moment} as shown in equation~\eqref{eq:raw-mom}.
\begin{equation}
  \label{eq:raw-mom}
  \mu_{r}(0) = \sum x^c P(x)
\end{equation}
It is straightforward to see that the first raw moment $\mu_{1}(0)=\sum
x P(x)$ is the mean value. Also note that the standard deviation
$\delta$ can be computed based on the first two moments: $\delta =
\sqrt{\mu_{2}(0) - \mu_{1}(0)^2}$.

\subsection{Hyperplane Voting}
\label{ssc:hyperVote}

Hains \textit{et al.} establish a method of exploiting hyperplane
averages to construct solutions to MAX-SAT that they call
\textit{hyperplane voting}.  In MAX-3SAT, given a clause $v_i$
containing the variables $p$, $q$, and $r$, there are eight possible
assignments of these variables: $'000'$, $'001'$, $'010'$, $'011'$,
$'100'$, $'101'$, $'110'$, and $'111'$.  The averages of the eight
hyperplanes formed by fixing $p$, $q$ and $r$ to each of these partial
assignments and leaving the remaining variables free are computed.  This
process is repeated for each clause in the instance.  Thus we compute
eight hyperplane averages for each clause for a total of $8m$ hyperplane
averages.  We then use the averages as scores to rank eight candidate
hyperplanes within each clause, and the candidate hyperplanes with
highest scores from every clause are used to calculate a probability
distribution over all $n$ variables as follows.

Let $\{p,q,r\}$ be the three variables in clause $v_i$.  Let $A_i : v_i
\mapsto \{0,1\}^3$ be the partial assignment of the variables in clause
$i$ that correspond to the hyperplane with the highest score for clause
$v_i$.  Let $\mathrm{true}_j$ count the number of times that variable
$j$ is set to 1 across all partial assignments $A$ and let
$\mathrm{total_j}$ count the total number of times that variable $j$
appears across all clauses (assignments).
%\[\mathrm{true}_j = |\{\forall i: j \in v_i \wedge A_i(j) = 1\} |\]
%Note that we are counting the elements in multi-sets; in other words, all
%assignments setting variable $j$ to are each counted by $\mathrm{true}_j$.
We define the following probability distribution over truth assignments
based on the hyperplane voting:
\[P(j=1) = \frac{\mathrm{true}_j}{\mathrm{total_j}}\]
Solutions are then constructed by generating a random value in the range
of $(0,1)$ for each variable.  If the random value generated for
variable $j$ is greater than $P(j=1)$, $j$ is set to 1, otherwise $j$ is
set to 0.

\subsection{Computing Any Moment over Hyperplanes}
\label{ssc:origMoment}

Let ${i}\in {x}$ denote the indices of value $'1'$ in ${i}$ is a subset
of that in ${x}$, where ${i}$, ${x}$ $\in \mathcal{B}^n$.  Heckendorn
\cite{heckendorn2002embedded} derives a closed-form formula for
computing any moment over a hyperplane $h$ as shown in
equation~\eqref{eq:oldComp}.
\begin{IEEEeqnarray}{rCl}
%\begin{align}
\label{eq:oldComp}
&& \mu_{r}^h (0)  \\
&=& \sum_{a_{1}\oplus a_{2}\oplus \dots \oplus a_{r} \subseteq
  \alpha(h)} w_{a_{1}} w_{a_{2}} \dots w_{a_{r}} (\psi_{a_{1}\oplus
  {a_{2}} \oplus \dots \oplus {a_{r}}}(\beta(h)) ), \nonumber 
%\end{align}
\end{IEEEeqnarray}
where $\oplus$ is the logical exclusive OR operator. In paper
\cite{heckendorn2002embedded} Heckendorn makes a statement about the
complexity of the computation in equation~\eqref{eq:oldComp}:
\begin{quote}
``The actual selection of the $a_i$'s makes the computation $O(n^r)$ where
n is the number of nonzero Walsh coefficients.'' (Page 367)
\end{quote}
Since Rana \textit{et al.} suggests the maximum number of nonzero Walsh
coefficients that a SAT/MAX-SAT instance can possibly generate is $m
2^k$, the ``$n$'' in Heckendorn's statement should be translated into
$m2^k$. In other words, Heckendorn claims a complexity of $O(2^{rk} m^r
)$ using equation~\eqref{eq:oldComp}.

\subsection{An Improved Computation}
\label{ssc:impr}

The key reason for the $O(2^{rk}m^r)$ bound corresponding to the
computation of the $r^{th}$ moment over hyperplanes is that every
nonzero Walsh coefficients are gone through exactly $r$ times for
checking the condition $a_{1}\oplus a_{2}\oplus \dots \oplus a_{r}
\subseteq \alpha(h)$. We argue that this is not necessary.  

\subsubsection{An Example}
\label{sss:exam}
We establish the new idea with a simple case where $r=2$. Let $Q$ be the
set of the indices of all nonzero Walsh
coefficients. Equation~\eqref{eq:oldComp} can be rewritten as equation
\begin{eqnarray}
\label{eq:oldComp2}
\mu_{2}^h(0) = \sum_{i\in Q \wedge j\in Q \wedge i\oplus j \subseteq
  \alpha(h)} w_{i} w_{j}  (\psi_{i\oplus {j}}(\beta(h)) ).
\end{eqnarray}
Consider a problem with 4 variables and the its relative $Q=\\ \{1010,
0101, 0010, 0001, 1000, 1110, 1011, 1101\}$, and $\alpha(h) = '0011'$.
We enumerate the results of $i\oplus j$ for all possible values for $i$
and $j$ in table~\ref{tab:exam}. Table~\ref{tab:exam} can also be viewed
as an enumeration of the iteration space for $i$ and $j$. Entries
colored in \textcolor{gray}{gray} are filtered out by condition $i
\oplus j \subseteq \alpha(h)$ and not actually included in the summation
in equation~\eqref{eq:oldComp2}. It is clear that there is a large
portion of iteration space (corresponding to the \textcolor{gray}{gray})
does not have to be visited and can be pruned out. To uncover the
pattern of \textcolor{gray}{gray} entries in table~\ref{tab:exam}, we
view the iteration on $i$ as outer loop and that on $j$ as inner loop,
and the idea also applies the other way around because of the symmetry
of the iteration space. Suppose $i='1010'$ (corresponding to the first
row of table~\ref{tab:exam}), the first two bits (counting from left to
right) of $j$ have to be $'10'$ in order to satisfy $i \oplus j
\subseteq \alpha(h)$. This suggests the only four possible values for
$j$ are $'1000'$, $'1001'$, $'1010'$ and $'1011'$. Moreover, only
$'1000'$ and $'1011'$ correspond to nonzero Walsh coefficients. The size
of the iteration space for $j$ while $i=1010'$ is effectively pruned
down to 3 from 8.

\begin{table}
\begin{center}
\begin{tabular}{|c|@{ }c@{ }|@{ }c@{ }|@{ }c@{ }|@{ }c@{ }|@{ }c@{ }|@{ }c@{ }|@{ }c@{ }|@{ }c@{ }|}
  \hline
  \diagbox{$i$}{$j$} &  1010  &  0101  &  0010  &  0001  &  1000  &  1110  &  1011  &  1101  \\
  \hline
  \hline
  1010  &                    0000  &  \textcolor{gray}{1111}  &  \textcolor{gray}{1000}  &  \textcolor{gray}{1011}  &                    0010  &  \textcolor{gray}{0100}  &                    0001  &  \textcolor{gray}{0111}  \\
  0101  &  \textcolor{gray}{1111}  &                    0000  &  \textcolor{gray}{0111}  &  \textcolor{gray}{0100}  &  \textcolor{gray}{1101}  &  \textcolor{gray}{1011}  &  \textcolor{gray}{1110}  &  \textcolor{gray}{1000}  \\
  0010  &  \textcolor{gray}{1000}  &  \textcolor{gray}{0111}  &                    0000  &                    0011  &  \textcolor{gray}{1010}  &  \textcolor{gray}{1100}  &  \textcolor{gray}{1001}  &  \textcolor{gray}{1111}  \\
  0001  &  \textcolor{gray}{1011}  &  \textcolor{gray}{0100}  &                    0011  &                    0000  &  \textcolor{gray}{1001}  &  \textcolor{gray}{1111}  &  \textcolor{gray}{1010}  &  \textcolor{gray}{1100}  \\
  1000  &                    0010  &  \textcolor{gray}{1101}  &  \textcolor{gray}{1010}  &  \textcolor{gray}{1001}  &                    0000  &  \textcolor{gray}{0110}  &                    0011  &  \textcolor{gray}{0101}  \\
  1110  &  \textcolor{gray}{0100}  &  \textcolor{gray}{1011}  &  \textcolor{gray}{1100}  &  \textcolor{gray}{1111}  &  \textcolor{gray}{0110}  &                    0000  &  \textcolor{gray}{0101}  &                    0011  \\
  1011  &                    0001  &  \textcolor{gray}{1110}  &  \textcolor{gray}{1001}  &  \textcolor{gray}{1010}  &                    0011  &  \textcolor{gray}{0101}  &                    0000  &  \textcolor{gray}{0110}  \\
  1101  &  \textcolor{gray}{0111}  &  \textcolor{gray}{1000}  &  \textcolor{gray}{1111}  &  \textcolor{gray}{1100}  &  \textcolor{gray}{0101}  &                    0011  &  \textcolor{gray}{0110}  &                    0000  \\
  \hline
\end{tabular}
\end{center}
\caption{Enumeration of $i \oplus j$ results for all values of $i$ and $j$. Suppose $\alpha(h) = '0011'$, the entries that do not satisfy $i\oplus j \subseteq \alpha(h)$ are colored in \textcolor{gray}{gray}.}\label{tab:exam}
\end{table}

\subsubsection{Runtime Complexity Analysis}
\label{sss:complexity}

Our new computation with iteration space pruning is formulated in
equation~\eqref{eq:comp2},
\begin{eqnarray}
\label{eq:comp2}
\mu_{2}^h(0) = \sum_{i\in Q} w_{i} \sum_{ i\oplus j \subseteq \alpha(h)} w_{j} \psi_{i\oplus j} (\beta(h)).
\end{eqnarray}
The idea is that for the inner most loop the pattern of values that can
possibly satisfy $i \oplus j \subseteq \alpha(h)$ is predictable so that
not every value of the inner most loop has to be visited. We now
establish a lemma regarding the runtime complexity of the new
computation.

\begin{lem}
  On a SAT/MAX-SAT instance, computing the second moment over a
  hyperplane selected based on clause by equation~\eqref{eq:comp2} takes
  $O(k 2^{2k} m)$ time.
\label{lem}
\end{lem}
\begin{proof}
  We analyze the complexity of computation in equation~\eqref{eq:comp2}
from the outside in: For the outer loop, there are $|Q|$ iterations for
varying $i$. Note that $\mathbf{bc}(\alpha(h)) = k$, since $h$ is
selected based on each clause and every clause contains at most $k$
variables. Thus there are $2^{k}$ possible values for j, and also
$2^{k}$ possible values for $i\oplus j$. For each j, $k$ from
$\alpha(h)$ positions need to be checked for the sign of Walsh term.
Overall, the cost for computing $\mu_{2}^{h}(0)$ by
equation~\eqref{eq:comp2} is $O(k* 2^k * |Q|) = O(k * 2^k * 2^{k} * m)
=O(k 2^{2k} m)$.
\end{proof}
The new bound $O(k 2^{2k} m)$ is better than the $O(2^{2k}m^2)$ as
suggested by Heckendorn in paper \cite{heckendorn2002embedded}, given
the fact that $k$ is typically much smaller than $m$ in most real-world
applications \cite{sat-competition}.


More generally, the new algorithm with iteration space pruning for
computing the $r^{th}$ moments over hyperplanes can be formulated in
equation
\begin{eqnarray}
\label{eq:comp}
&&\mu_{r}^h(0) \nonumber \\
&=&\sum_{ a_{1}\in Q} w_{a_{1}} \sum_{a_{2} \in Q} w_{a_{2}} \dots \sum_{a_{1} \oplus a_{2} \oplus \dots \oplus a_{r} \subseteq \alpha(h)} \nonumber\\
&& w_{a_{r}}  (\psi_{a_{1} \oplus a_{2} \oplus \dots \oplus a_{r}}(\beta(h))).
\end{eqnarray}

Theorem~\ref{thm} settles the runtime complexity for the general case of
computing moments over hyperplanes.
\begin{thm}
  On a SAT/MAX-SAT instance, computing the $r^{th}$ moment over a
  hyperplane selected by equation~\eqref{eq:comp} takes $O(k 2^{2k} m)$
  time.
  \label{thm}
\end{thm}
\begin{proof}
  The iteration pruning idea applies only to the innermost loop,
  therefore full iteration over the set $Q$ is still required the each
  of the first $(r-1)$ levels of loops. The cost for this part is
  $|Q|^{r-1} = (2^{k}m)^{c-1}$. For the innermost loop, the iteration
  space is reduced to $O(k2^k)$ by lemma~\ref{lem}. Overall, the cost of
  computing the $r^{th}$ moment over a hyperplane according to
  equation~\eqref{eq:comp} is $O(k2^{kc}m^{c-1})$.
\end{proof}
Again, the new result is an improvement over the bound $O(2^{rk} m^r )$
claimed by Heckendorn \cite{heckendorn2002embedded}.  In practice, we
found our new algorithm computes the second moment over a hyperplane on
a random MAX-3SAT instance with 250 variables and 1065 clauses in 0.047
seconds while the old algorithm takes 34.3 seconds. This implies an
impressive speedup of \textit{729 times}. Note that the all data on
running time in this paper were collected on a machine with Intel(R)
Core(TM) i5-2430M CPU @ 2.40GHz and 6GB main memory running Debian
GNU/Linux jessie/sid version. All algorithms were implemented in
C\footnote{The source code can be check-outed at
  \url{https://bitbucket.org/qzcwx/hgls}} and compiled using gcc 4.7.3.

% To shed some lights on what impact of the improvement of complexity
% result on actual running time,

\textbf{Corollary 1}: Hyperplane voting requires the moments over $2^k$
candidate hyperplanes for every clause ($m$ clauses in total), therefore
hyperplane voting using the $r^{th}$ moments as scores for ranking
hyperplanes takes $O(2^k * m * k2^{kc}m^{c-1}) = O(k2^{k(c+1)}m^c)$.

\section{Application to MAX-SAT}
\label{sec:appl}
Our improvement on the efficiency of computing higher order moments
introduced in the previous section enables us to apply the two new
scores to instances of greater sizes. Now that it is possible to compute
second moments over hyperplanes efficiently using the new algorithm, we
next consider how the second moments can be used to better evaluate the
potential of a candidate hyperplane containing a global optimum. In this
section, we design two new scores on which the hyperplane voting process
is based to select top hyperplane for each clause.  The two new scores
are built on the basis of second moments over hyperplanes.

\subsection{A Motivating Example}
\label{sse:movit}

Suppose in figure~\ref{fig:dist} the three curves with different colors
represent the fitness distributions associated with three candidate
sub-search-spaces and we are interested in maximizing fitness. The mean
values of three curves are all the same, while the yellow one is the
best candidate hyperplane. In this case, using the mean value over
hyperplane as score can fail to guide search to reach the optimum.
\begin{figure}
  \centering
  \includegraphics[width=0.45\textwidth]{dist.pdf}
  \caption{Example Fitness Distributions over Hyperplanes
    \protect\footnotemark}
  \label{fig:dist}
\end{figure}

\footnotetext{Modified from
  \url{http://en.wikipedia.org/wiki/File:Normal_Distribution_PDF.svg}}

\subsection{$\delta$ and $\mu_1(0)+3\delta$ as scores for ranking
  hyperplanes}
\label{sse:score}
From figure~\ref{fig:dist}, we found that if the mean values of the
several distributions are closet to each other, the distribution with
higher standard deviation is likely to contain higher maximum
value. 

We argue that this may be case for distributions over hyperplanes on
MAX-3SAT instances. Recall that we are ranking 8 candidate hyperplanes
involving 3 variables. On a random MAX-3SAT instance, the 3 variables
will appear in at most 9 clauses in expectation. It suggests that
altering the partial assignments to any 3 variables will change the
evaluation of a solution (the number of satisfied clauses) by at most
9. Compared to the total number of clauses $m$, 9 is a small and bounded
number for any non-trivial problem. Second, the mean values over a
candidate hyperplane is the average over $2^{n-3}$ solutions. The effect
of the partial assignments of 3 variables is further diluted. In short,
the mean values for all candidate hyperplanes associated with a clause
should be approximately the same. Consequently, instead of using the
mean value as the score for ranking hyperplane, we propose to use the
standard deviation $\delta$ over hyperplane as score.

Assuming a normal distribution, only 0.135\% of samples will be greater
than $\mu_1(0)+3\delta$. We thus consider $\mu_1(0)+3\delta$ as a
suitable estimator for the maximum value of the distribution over a
hyperplane.

\section{Empirical Study}
\label{sec:empr}

Except for studies focusing on search space analysis
\cite{DBLP:journals/jair/SingerGS00, Frank97whengravity}, the most
common practice for evaluating SLS is to look at the evaluation of the
best-found solution. Nevertheless, because of the basins of attraction,
SLS may be trapped in some local optima with good evaluations that are
extremely difficult to escape to reach global optima.  In the current
paper, we would like to view the quality of a solution by both
evaluation and its Hamming distance to nearest global optimum.


In order to calculate the distance from a solution to near global
optima, we have to know all global optima first. We employ MiniMAXSAT
\cite{DBLP:journals/jair/HerasLO08}, a complete MAX-SAT solver, to find
a global optima. We denote its assignments as $sol_o$ and its evaluation
as $eval_o$.  The negation of $sol_o$ is then appended as a new clause
to the original instance. This way we make sure $sol_o$ will not be
rediscovered.  We then rerun MiniMAXSAT on the new instance to find a
new global optima. This process is repeated until the evaluation of the
newly found global optima is worse than $eval_o$, which suggests all
global optima of the original instances have been found.

\subsection{Experiment Setup}
\label{sse:setup}

We generate 30 random 3SAT instances with $n=50$ and $m= 215, 300, 400,
500$ (120 instances in total). Hyperplane voting with three different
scores (i.e., $\mu_1^h(0)$, $\mu_1^h(0)+3\delta$ and $\delta$) are
performed to on these instances. Random initialization is also tested on
all instances as a baseline. We then have four methods of generating $P$
for initializing SLS. Each $P$ is used to generate 100 initial
solutions. 

\subsection{Time Spent on Generating $P$}
\label{sse:time}
Before we investigate the impact of the new score on SLS, we need to
know the cost of adopting new scores in hyperplane voting to generate
$P$. Table~\ref{tab:time} lists the means and standard deviations for
each of the four different methods of generating $P$. For random
initialization, $P$ is constructed with all 0.5s, so that every variable
has equal probability to be initialized to 0 or 1. There is no other
extra cost for generating $P$ in case of random initialization. As
expected, table~\ref{tab:time} shows almost no time for random
initialization method to generate $P$.

On the other hand, the three hyperplane voting based methods, have an
average processing time ranging from 0.015 seconds to 66.7
seconds. According to Corollary 1, time for performing hyperplane voting
using $\mu_0^h(0)$ (denoted as $T_1$) is $O(k2^{2k}m) = O(3*2^6m) =
O(192m)$, while that for using $\mu_0^h(0)+3\delta$ or $\delta$ (denoted
as $T_2$) takes $O(k2^{3k}m^2) = O(3*2^9 m^2) = O(1536m^2)$. 

We first comparing $T_1$ with $T_2$ in table~\ref{tab:time}, namely the
scalability of hyperplane voting on $r$. In theory, we expect
$\frac{T_2}{T_1} = O(\frac{1536m}{192m}) = O(8m)$. We find in
table~\ref{tab:time} that the actual running time scales a bit better on
$r$ than the theory indicates. On instance uf-n50-m215, for example,
$\frac{1.47e+01}{1.51e-02}=973.5$, while $O(8m)$ in this case is as much
as $1720$.


% > [1] 65.9
% >  6.67e+01 / 1.48e+01
% [1] 4.506757
% > 500/215
% [1] 2.325581
% > (500/215)^@
% Error: unexpected '@' in "(500/215)^@"
% > (500/215)^2
% [1] 5.408329

We then study the actual impact of parameter $m$ on $T_2$. We again find
that the actual scalability of $T_2$ on $m$ is slightly better than the theory
suggests. Take the $\delta$ method in table~\ref{tab:time} for example,
the increase of $T_2$ from uf-n50-m215 to uf-n50-m500 is
$\frac{6.67e+01}{1.48e+01} = 4.5$ times. The theory suggests $T_2$
scales \textit{quadratically} with $m$, i.e., the increase in this case
is $\frac{500^2}{215^2}=5.4$ times.

\begin{table}[tb]
\begin{center}
\begin{tabular}{ccc}
\hline
instance         &  method                &   time   \\
\hline
\hline
 uf-n50-m215  &  random                &  1.20e-06 $\pm$ 4.07e-07  \\
                  &  $\mu_0^h(0)$          &  1.51e-02 $\pm$ 2.96e-03  \\
                  &  $\mu_0^h(0)+3\delta$  &  1.47e+01 $\pm$ 3.16e-01  \\
                  &  $\delta$              &  1.48e+01 $\pm$ 5.15e-01  \\
\hline
 uf-n50-m300  &  random                &  1.43e-06 $\pm$ 5.04e-07  \\
                  &  $\mu_0^h(0)$          &  3.11e-02 $\pm$ 5.63e-02  \\
                  &  $\mu_0^h(0)+3\delta$  &  2.60e+01 $\pm$ 5.02e-01  \\
                  &  $\delta$              &  2.60e+01 $\pm$ 4.55e-01  \\
\hline
 uf-n50-m400  &  random                &  1.63e-06 $\pm$ 1.19e-06  \\
                  &  $\mu_0^h(0)$          &  2.36e-02 $\pm$ 1.53e-03  \\
                  &  $\mu_0^h(0)+3\delta$  &  4.34e+01 $\pm$ 1.72e+00  \\
                  &  $\delta$              &  4.37e+01 $\pm$ 2.17e+00  \\
\hline
uf-n50-m500  &  random                &  1.50e-06 $\pm$ 5.09e-07  \\
                  &  $\mu_0^h(0)$          &  3.31e-02 $\pm$ 8.81e-03  \\
                  &  $\mu_0^h(0)+3\delta$  &  6.67e+01 $\pm$ 3.64e+00  \\
                  &  $\delta$              &  6.59e+01 $\pm$ 4.08e+00  \\
\hline
\end{tabular}
\end{center}
\caption{Time in seconds spent on generating $P$. The means and standard deviations over 30 instances for each configuration are reported.}\label{tab:time}
\end{table}


As discussed before, an extra $O(8m)$ times cost is required in
hyperplane voting if we utilize second mounts in place of first
moments. The natural question is to ask if the extra cost is worthwhile.


\subsection{Initial Solutions}
\label{sse:init}

We compare the 100 initial solutions constructed by $P$ using four
different methods on 120 instances described in
subsection~\ref{sse:setup}, and statistics about the numbers of
unsatisfied clauses and the nearest distances from the initial solutions
to nearest global optima over 30 instances for each parameter
configuration are reported in table~\ref{tab:initEval}.  



\begin{table}[tb]
\begin{center}
\begin{tabular}{cccc}
\hline
 instance     &  method                &  evaluation  &  dist. opt.  \\
\hline
\hline
 uf-n50-m215  &  random                &  27.13$\pm$4.85  &  21.11$\pm$3.32  \\
              &  $\mu_0^h(0)$          &   12.0$\pm$2.74  &  13.92$\pm$3.01  \\
              &  $\mu_0^h(0)+3\delta$  &    8.6$\pm$2.86  &  11.64$\pm$3.67  \\
              &  $\delta$              &   42.8$\pm$6.67  &  20.79$\pm$2.74  \\
\hline
 uf-n50-m300  &  random                &  37.31$\pm$6.09  &  22.38$\pm$3.75  \\
              &  $\mu_0^h(0)$          &   19.9$\pm$3.53  &  15.21$\pm$3.78  \\
              &  $\mu_0^h(0)+3\delta$  &   15.1$\pm$3.46  &  13.27$\pm$5.41  \\
              &  $\delta$              &   52.6$\pm$7.78  &  21.50$\pm$3.82  \\
\hline
 uf-n50-m400  &  random                &  50.29$\pm$6.49  &  23.52$\pm$3.73  \\
              &  $\mu_0^h(0)$          &   28.9$\pm$4.00  &  15.56$\pm$3.81  \\
              &  $\mu_0^h(0)+3\delta$  &   22.3$\pm$4.41  &  12.90$\pm$5.15  \\
              &  $\delta$              &   66.1$\pm$9.18  &  19.92$\pm$2.73  \\
\hline
 uf-n50-m500  &  random                &  62.83$\pm$7.39  &  23.21$\pm$3.59  \\
              &  $\mu_0^h(0)$          &   35.4$\pm$6.14  &  14.00$\pm$3.67  \\
              &  $\mu_0^h(0)+3\delta$  &   29.0$\pm$6.46  &  12.25$\pm$4.44  \\
              &  $\delta$              &  76.8$\pm$13.32  &  19.72$\pm$3.95  \\
\hline
\end{tabular}
\end{center}
\caption{Statistics about the numbers of unsatisfied clauses of initial solutions (``evaluation'' column) and their distance to nearest global optima (``dist. opt.'' column). The mean values and standard deviations over 30 instances and 100 runs are reported.}\label{tab:initEval}
\end{table}

It is clear from table~\ref{tab:initEval} that hyperplane voting with
$\mu_0^h(0) + 3\delta$ consistently produce the best initial solutions
both in terms of evaluations and distances to nearest global
optima. Utilizing second moments indeed leads to better initial
solutions: they are better in evaluation and closer to nearest global
optima. This superiority appears to be independent of the c/v ratios of
the uniform random MAX-3SAT instances.

Interestingly, even though hyperplane voting with $\delta$ has the worst
evaluation among the four methods (even worse than uniform random
initialization), the distances to nearest global optima is actually
smaller than that of uniform random initialization. This suggests that
search space of MAX-3SAT is deceptive for SLS. SLS typically follows the
gradient in evaluation and may be lead further away from actual global
optima although solutions with better evaluations are discovered. Such
deception has also been reported by Hoos \textit{et al.}  \cite{raey}
using problem difficulty measures such as autocorrelation and fitness
distance correlation.

Note that these problem difficulty measures are designed for SLS, and
may be not suitable for heuristics using global information regarding
problem structures, such as hyperplane voting that is capable to analyze
and utilize the statistical moments across the entire search space. As a
consequence, hyperplane voting with $\mu_1^h(0)$ constructs initial
solutions that are not only better in evaluations but also closer to
global optima.

As suggested by Hains \textit{et al.} \cite{hyper2013gecco}, hyperplane
voting can be also employed to estimate the values of backbone
variables. According to their study, hyperplane voting with $\mu_1^h(0)$
biases the initial assignment towards the backbone in 121 of the 163
backbone variables found across 10 random 3SAT instances with $n=20$ and
$m=85$. Their study are limited on small instances with $n=20$, due to
enumeration of search space. Using the modified MiniMAXSAT, however, we
are able to perform studies on instances with $n=50$ and various c/v
ratios.

Table~\ref{tab:initbb} presents statistics regarding the backbone sizes
and how far away for each of the four methods to set all backbone
variables correctly. The superiority of hyperplane voting with
$\mu_0^h(0)+3\delta$ among all four methods regarding the initialization
of all variables (as shown in table~\ref{tab:initEval}) still holds
here. Indeed, from the perspective of correctly setting backbone
variables, using second moments is again able to further boost the
performance of hyperplane voting. The property of being backbone
variables does not appear to affect the advantage of utilizing second
moments in place of first moments. Also, just as expected, uniform
randomly initialized solutions correctly set about half of backbone
variables.

\begin{table}[tb]
\begin{center}
\begin{tabular}{cccc}
\hline
 instance     &  method                &  backbone size  &   dist. bb.  \\
\hline
\hline
 uf-n50-m215  &  random                &    23.00$\pm$16.74  &  11.30$\pm$7.19  \\
              &  $\mu_0^h(0)$          &                 &   5.65$\pm$4.38  \\
              &  $\mu_0^h(0)+3\delta$  &                 &   3.93$\pm$3.19  \\
              &  $\delta$              &                 &  10.37$\pm$5.70  \\
\hline
 uf-n50-m300  &  random                &    33.29$\pm$14.08  &  13.40$\pm$8.97  \\
              &  $\mu_0^h(0)$          &                 &   7.96$\pm$8.58  \\
              &  $\mu_0^h(0)+3\delta$  &                 &   7.87$\pm$8.80  \\
              &  $\delta$              &                 &  12.95$\pm$9.14  \\
\hline
 uf-n50-m400  &  random                &    42.00$\pm$13.47  &  21.75$\pm$7.05  \\
              &  $\mu_0^h(0)$          &                 &  13.05$\pm$6.20  \\
              &  $\mu_0^h(0)+3\delta$  &                 &  10.02$\pm$6.16  \\
              &  $\delta$              &                 &  17.01$\pm$5.18  \\
\hline
 uf-n50-m500  &  random                &    35.14$\pm$15.38  &  19.17$\pm$5.55  \\
              &  $\mu_0^h(0)$          &                 &   9.51$\pm$6.02  \\
              &  $\mu_0^h(0)+3\delta$  &                 &   7.45$\pm$5.98  \\
              &  $\delta$              &                 &  14.55$\pm$6.06  \\
\hline
\end{tabular}
\end{center}
\caption{Statistics about the numbers of backbone variables in a instance (``backbone size'') and distances from initial solutions to backbone variables (the numbers of backbone variables incorrectly set by initial solutions). The mean values and standard deviations over 30 instances and 100 runs are reported.}\label{tab:initbb}
\end{table}

\subsection{First Local Optima}
\label{sse:firstLoc}

Now that we have demonstrated the superiority of hyperplane voting with
$\mu_0^h(0)+3\delta$, it would be interesting to examine whether this
superiority will still hold after applying SLS. Moreover, as we
discussed in the previous subsection, SLS search utilizes \textit{local}
gradient in evaluation while hyperplane voting exploits \textit{global},
it is intriguing to uncover how the two strategies with very discrepant
emphases work together. SLS can improve the evaluation of solutions for
sure, but can it brings the initial solutions generated by hyperplane
voting even closer to global optima?



\section{Conclusion and Future Work}
\label{sec:con}



\bibliographystyle{plain}
\bibliography{references}

\end{document}
