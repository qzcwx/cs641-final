# table for genrating org table, which is further translated into latex
# table 

print
a = ['1010', '0101', '0010', '0001', '1000', '1110', '1011', '1101']
alpha = '0011'

zeroI = []
for i in range(len(alpha)):
    if alpha[i] == '0':
        zeroI.append(i)

print zeroI

for i in a:
    print '|',i,'|',
    for j in a:
        s = ''
        for k in range(len(j)):
            if i[k]==j[k]:
                s = s + '0'
            else:
                s = s + '1'
        grey = False 
        for k in zeroI:
            if s[k] == '1':
                grey = True
                break

        if grey == True:
            print '\\textcolor{grey}{%s}|' %(s),
        else:
            print s, '|',
    print
